function Soal0106(n = 1) {
    var arr2d = GetArray2D(n, n * 3);

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n * 3; j++) {
            if (i == 0) {
                if ((j + n) % n == 0) {
                    arr2d[i][j] = "*";
                } else {
                    arr2d[i][j] = " ";
                }
            }

            if (i == n - 1) {
                if ((j + n) % n == 0) {
                    arr2d[i][j] = "O";
                } else {
                    arr2d[i][j] = "*";
                }
            }
                if (i > 0 && i < n - 1) {
                    if ((j + 1) % n == 0) {
                        arr2d[i][j] = " ";
                    } else {
                        arr2d[i][j] = "*";
                    }
                }
            
        }
    }
    return Print(arr2d)
}