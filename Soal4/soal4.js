function Soal0104(str = "Aku") {
    var berubah = str.split(' ');
    var arrKata = [];

    for (let row = 0; row < berubah.length; row++) {
        var newKata = '';
        var s = berubah[row];

        for (let col = 0; col < s.length; col++) {
            if (col > 0 && col < s.length -1) {
                if(col>0 && col<=4){
                    newKata += "*";
                }
               
            } else {
                newKata += s[col];
            }
        }
        arrKata[row] = newKata;
    }
    return arrKata.join(" ");
}