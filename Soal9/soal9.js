function Soal0109(n) {
    var arr2d = GetArray2D(n, n);
    var a = 3;

    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n ; j++) {
            if (i == j || (n - i - 1) == j) {
                arr2d[i][j] = " ";
            }

            else {
                if ((i + 2) % 2 == 0) {
                    arr2d[i][j] = a*(i+1);
                }
                else {
                    arr2d[i][j] = a*(i+1)/ 3;
                }
            }
        }

    }
    return Print(arr2d)
}